import React, {Component} from 'react';
import {defaultStyle, pages} from './assets/content/conf';
import './App.css';
import {MessageComponent} from './MessageComponent';

class App extends Component {

  constructor(props) {
    super(props);
    this.conf = pages;
    this.state = {
      userType: 0,
      currentView: 'entry',
      backgroundImage: pages['entry'].image,
      viewList: defaultStyle.views,
      displayMessage: false,
      messageToDisplay: ""
    };
    this.botLeft = {
      top: '2%',
      left: '2%'
    }
  }

  render() {
    let vue;
    const defaultImgStyle = defaultStyle.imgStyle;
    const defaultAppStyle = defaultStyle.appStyle;
    const defaultBtnStyle = defaultStyle.defaultBtnStyle;
    switch (this.state.userType) {
      case 0:
        const leftBtnStyle = this.mergeElement(defaultBtnStyle, pages.entry.leftButton.style);
        const rightBtnStyle = this.mergeElement(defaultBtnStyle, pages.entry.rightButton.style);
        const leftNextView = pages.entry.leftButton.nextView;
        const rightNextView = pages.entry.rightButton.nextView;
        vue = (
          <div>
            <button style={leftBtnStyle}
                    onClick={this.setUserStatus.bind(this, 1, leftNextView)}>{pages.entry.leftButton.txt}</button>
            <button style={rightBtnStyle}
                    onClick={this.setUserStatus.bind(this, 2, rightNextView)}>{pages.entry.rightButton.txt}</button>
          </div>
        );
        break;
      default:
        const buttons = this.getButtonSetup(this.conf);
        const styleBotLeft = this.mergeElement(defaultBtnStyle, this.botLeft);
        const returnMenu = this.state.currentView === 'home'? '': (
          <button style={styleBotLeft} onClick={this.returnMenu.bind(this)}>
            Accueil
          </button>);
        vue = (
          <div>
            <div style={defaultAppStyle}>
              <div>
                {buttons}
              </div>
            </div>
            <MessageComponent
              buttonName={this.state.lastButtonClicked}
              display={this.state.displayMessage}
              message={this.state.messageToDisplay}
              hideMessageBox={this.hideMessageBox.bind(this)}/>
            {returnMenu}
          </div>
        );
        break;
    }

    return (
      <div className="App">
        <img style={defaultImgStyle} src={this.state.backgroundImage}>

        </img>
        {vue}
      </div>
    );
  }

  setUserStatus = (status, nextView) => {
    const image = pages[nextView].image;
    this.setState({
      userType: status,
      backgroundImage: image,
      currentView: nextView
    });
  };

  getButtonSetup = (conf) => {
    const defaultBtnStyle = defaultStyle.defaultBtnStyle;
    const vue = [];
    if (conf[this.state.currentView]['format']) {
      for (const buttonName of conf[this.state.currentView]['format'].btn) {
        const current = conf[this.state.currentView][buttonName];
        let style;
        if (this.state.userType === 1) {
          style = this.mergeElement(defaultBtnStyle, current.style);
        } else {
          if (current.proStyle !== undefined && current.proStyle !== {} && current.proStyle !== null) {
            style = this.mergeElement(defaultBtnStyle, current.proStyle);
          } else {
            style = this.mergeElement(defaultBtnStyle, current.style);
          }
        }
        const studentMessage = current.studentMessage;
        const proMessage = current.proMessage;
        const txt = current.buttonText;
        vue.push((
          <button style={style} onClick={this.renderVueMessage.bind(this, buttonName, studentMessage, proMessage)}>
            {txt}
          </button>
        ));
      }
    }
    return vue;
  };

  renderVueMessage = (buttonName, studentMessage, proMessage) => {
    const nextView = pages[this.state.currentView][buttonName].nextView;
    if (nextView && nextView !== '' && nextView !== null) {
      const nextImage = pages[nextView].image ?
        pages[nextView].image:
        this.state.backgroundImage;
      this.setState({
        displayMessage: false,
        lastButtonClicked: buttonName,
        currentView: nextView,
        backgroundImage: nextImage
      });
    } else {
      switch (this.state.userType) {
        case 1:
          this.displayMessageBox(studentMessage);
          break;
        default:
          this.displayMessageBox(proMessage);
          break;
      }
    }
  };

  displayMessageBox = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      this.setState({
        displayMessage: true,
        messageToDisplay: message
      });
    } else {
      this.setState({
        displayMessage: false
      });
    }
  };

  hideMessageBox = () => {
    this.setState({
      displayMessage: false
    });
  };

  mergeElement = (a, b) => {
    return Object.assign({}, a, b);
  };

  returnMenu = () => {
    const image = pages.home.image;
    this.setState({
      currentView: 'home',
      backgroundImage: image,
      displayMessage: false
    });
  };
}

export default App;
