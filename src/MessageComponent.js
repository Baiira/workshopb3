import React, {Component} from 'react';

export class MessageComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            display: false,
            currentMessage: 0,
            style: {
                backgroundColor: '#dedede',
                opacity: '0.9',
                border: '1px',
                shadowOffsetX: '5px',
                shadowOffsetY:'4px',
                borderRadius: '5px',
                borderStyle: 'outset',
                height: '90%',
                width: '30%',
                top: '5%',
                right: '1%',
                position: 'fixed'
            },
            btnStyle: {
                backgroundColor: '#999999',
                borderWidth: '1px',
                borderRadius: '3px',
                position: 'absolute',
                bottom: '19px',
                left: '45%'
            },
            btnStyleNext: {
                backgroundColor: '#999999',
                borderWidth: '1px',
                borderRadius: '3px',
                position: 'absolute',
                bottom: '19px',
                right: '5%'
            },
            btnStylePrev: {
                backgroundColor: '#999999',
                borderWidth: '1px',
                borderRadius: '3px',
                position: 'absolute',
                bottom: '19px',
                left: '5%'
            }
        };
    }

    render() {
        let vue;
        let prevBtn;
        let nextBtn;
        if (this.props.display) {
            if (this.props.message[this.state.currentMessage + 1]) {
                nextBtn = (
                    <button style={this.state.btnStyleNext} onClick={this.nextMessage.bind(this)}>
                        Suivant
                    </button>
                );
            }
            if (this.props.message[this.state.currentMessage - 1]) {
                prevBtn = (
                    <button style={this.state.btnStylePrev} onClick={this.previousMessage.bind(this)}>
                        Précédent
                    </button>
                );
            }
            if (this.props.message[this.state.currentMessage]) {
                vue = (
                    <div style={this.state.style}>
                        {this.props.message[this.state.currentMessage]}
                        <button style={this.state.btnStyle} onClick={this.props.hideMessageBox.bind(this)}>
                            Fermer
                        </button>
                        {nextBtn}
                        {prevBtn}
                    </div>
                );
            } else {
                setTimeout(
                    this.setState({
                        currentMessage: 0
                    }), 0);
            }
        } else {
            vue = (
                <div>
                </div>
            );
        }
        return (
            <div>
                {vue}
            </div>
        );
    }

    nextMessage = () => {
        if (this.props.message[this.state.currentMessage + 1]) {
            this.setState({
                currentMessage: this.state.currentMessage + 1
            })
        }
    };

    previousMessage = () => {
        if (this.props.message[this.state.currentMessage - 1]) {
            this.setState({
                currentMessage: this.state.currentMessage - 1
            })
        }
    };
}
