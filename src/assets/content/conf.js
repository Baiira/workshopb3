import React from 'react';

export const defaultStyle = {
    imgStyle: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        height: '100%',
        width: '100%',
        zIndex: -1
    },
    appStyle: {
        zIndex: '1000',
        position: 'fixed',
        top: '600px',
        left: '50%'
    },
    defaultBtnStyle: {
        width: '100px',
        height: '100px',
        background: '#fafafa',
        boxShadow: '2px 2px 8px #aaa',
        font: 'bold 13px Arial',
        borderRadius: '50%',
        color: '#555',
        position: 'fixed',
        top: '48%'
    },
    views: [
        'entry', 'home', 'lifespace', 'workspace', 'mydil', 'admission', 'partnership'
    ]
};

export const pages = {

    entry: {
        image: require('../images/Entrée Apothicaire.jpg'),
        leftButton: {
            style: {

                left: '33%',
                top: '45%'
            },
            nextView: 'home',
            txt: 'Étudiant'
        },
        rightButton: {
            style: {

                left: '33%',
                top: '5%'
            },
            nextView: 'home',
            txt: 'Professionel'
        }
    },

    home: {
        image: require('../images/Couloir Entrée Apothicaire.jpg'),
        btn1: {
            style: {
                top: "60%",
                left: "42%"
            },

            studentMessage: [
                (<p>
                    <h2>PRESENTATION</h2>
                    Première école d’informatique en France avoir été créée par des professionnels, première à avoir
                    obtenu
                    le titre niveau I RNCP (Bac+5 certifié par l’État) et résolument engagée auprès des entreprises,
                    l’EPSI
                    accompagne le développement des compétences informatiques depuis plus de 55 ans.
                    EPSI est membre de HEP Education. Nous partageons ensemble des valeurs et attitudes fortes,
                    ouverture au
                    monde, accompagnement, humanisme, solidarité, innovation, convivialité.
                    <h5>Notre mission est de former et certifier des professionnels à haut niveau d’expertise dans les
                        métiers de l’informatique</h5>
                    <ul>
                        <li>Experts de la conduite de projet</li>
                        <li>Experts en systèmes d’information et ERP management</li>
                        <li>Experts en développement logiciel</li>
                        <li>Experts en sécurité, infrastructure et réseaux</li>
                        <li>DevOps</li>
                    </ul>
                </p>)
            ],
            proMessage: [
                (<p>
                    <h2>PRESENTATION</h2>
                    Première école d’informatique en France avoir été créée par des professionnels, première à avoir
                    obtenu
                    le titre niveau I RNCP (Bac+5 certifié par l’État) et résolument engagée auprès des entreprises,
                    l’EPSI
                    accompagne le développement des compétences informatiques depuis plus de 55 ans.
                    EPSI est membre de HEP Education. Nous partageons ensemble des valeurs et attitudes fortes,
                    ouverture au
                    monde, accompagnement, humanisme, solidarité, innovation, convivialité.
                    <h5>Notre mission est de former et certifier des professionnels à haut niveau d’expertise dans les
                        métiers de l’informatique</h5>
                    <ul>
                        <li>Experts de la conduite de projet</li>
                        <li>Experts en systèmes d’information et ERP management</li>
                        <li>Experts en développement logiciel</li>
                        <li>Experts en sécurité, infrastructure et réseaux</li>
                        <li>DevOps</li>
                    </ul>
                </p>)
            ],
            buttonText: "Présentation"
        },
        btn2: {
            style: {
                top: "30%",
                left: "35%"
            },
            proStyle: {
                display: "none"
            },
            nextView: 'mydil',
            buttonText: 'MyDIL'
        },
        btn3: {
            style: {
                top: "45%",
                left: "42%"
            },
            proStyle: {
                display: "none"
            },
            nextView: 'lifespace',
            buttonText: 'Espace de Vie'
        },
        btn4: {
            style: {
                top: "30%",
                left: "49%"
            },
            nextView: 'workspace',
            buttonText: 'Espace de Travail'
        },
        btn5: {
            style: {
                top: "40%",
                left: "28%"
            },
            nextView: 'partnership',
            buttonText: 'Entreprises'
        },
        btn6: {
            style: {
                top: "50%",
                left: "21%"
            },
            proStyle: {
                display: 'none'
            },
            nextView: 'admission',
            buttonText: 'Admissions'
        },
        format: {
            btn: [
                "btn1", "btn2", "btn3", "btn4", "btn5", "btn6"
            ]
        }
    },

    lifespace: {
        image: require('../images/Espacedetente.jpg'),
        btn1: {
            style: {
                top: "50%",
                left: "50%"
            },
            proStyle: {
                display: "none"
            },
            buttonText: 'Espace Détente',
            studentMessage: [(
                <p>Un espace est mit à disposition pour les élèves, afin de leurs permettrent de souffler</p>)]
        },
        format: {
            btn: [
                "btn1"
            ]
        }
    },

    workspace: {
        image: require('../images/Salle de cours 2.jpg'),
        btn1: {
            style: {
                top: "10%",
                left: "30%"
            },

            buttonText: 'BACHELOR 1',
            studentMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Apprendre à développer (algorithmique)</li>
                        <li>Langages de développement : C, C++</li>
                        <li>Langages de développement web (php, html, css, mySQL)</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau gestion des
                        données</h4>
                    <ul>
                        <li>Environnements systèmes (Windows – Linux)</li>
                        <li>Réseaux et topologies</li>
                        <li>IP V4 et V6</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Mathématiques</li>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                        <li>Modélisation de bases de données (Merise – Uml)</li>
                        <li>Conception et adaptation d’une base de données (SQL Server)</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Mathématiques pour l’informatique</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),
                (<p>
                    <h2>PARCOURS BTS SIO (EN OPTION AVEC SOUSCRIPTION DU PACK)</h2>
                    <ul>
                        <li>Algorithmique et mathématiques</li>
                        <li>Analyse économique, managériale et juridique</li>
                        <li>Projets personnalisés encadrés (PPE)</li>
                    </ul>
                    <h4>STAGE EN ENTREPRISE DE 4 A 5 SEMAINES</h4>
                    <h4>COMPÉTENCES À DÉVELOPPER</h4>
                    <ul>
                        <li>Gérer des incidents et problèmes (assistance utilisateur)</li>
                        <li>Développer une solution d’infrastructure</li>
                        <li>Développer et tester une application Objet</li>
                        <li>Développer une application web ou nomade</li>
                        <li>Administrer et superviser une solution d’infrastructure</li>
                        <li>Produire une documentation technique</li>
                    </ul>
                    <h2>Validation de la 1ère année</h2></p>)
            ],
            proMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Apprendre à développer (algorithmique)</li>
                        <li>Langages de développement : C, C++</li>
                        <li>Langages de développement web (php, html, css, mySQL)</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau gestion des
                        données</h4>
                    <ul>
                        <li>Environnements systèmes (Windows – Linux)</li>
                        <li>Réseaux et topologies</li>
                        <li>IP V4 et V6</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Mathématiques</li>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                        <li>Modélisation de bases de données (Merise – Uml)</li>
                        <li>Conception et adaptation d’une base de données (SQL Server)</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Mathématiques pour l’informatique</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),
                (<p>
                    <h2>PARCOURS BTS SIO (EN OPTION AVEC SOUSCRIPTION DU PACK)</h2>
                    <ul>
                        <li>Algorithmique et mathématiques</li>
                        <li>Analyse économique, managériale et juridique</li>
                        <li>Projets personnalisés encadrés (PPE)</li>
                    </ul>
                    <h4>STAGE EN ENTREPRISE DE 4 A 5 SEMAINES</h4>
                    <h4>COMPÉTENCES À DÉVELOPPER</h4>
                    <ul>
                        <li>Gérer des incidents et problèmes (assistance utilisateur)</li>
                        <li>Développer une solution d’infrastructure</li>
                        <li>Développer et tester une application Objet</li>
                        <li>Développer une application web ou nomade</li>
                        <li>Administrer et superviser une solution d’infrastructure</li>
                        <li>Produire une documentation technique</li>
                    </ul>
                    <h2>Validation de la 1ère année</h2></p>)
            ]
        },
        btn2: {
            style: {
                top: "25%",
                left: "30%"
            },

            buttonText: 'BACHELOR 2',
            studentMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Langages de développement Objet et Tests unitaires (C++, C#)</li>
                        <li>Conception d’IHM web et nomade (PHP symfony, javascript, Jquery, UX Design…)</li>
                        <li>Langages de développement web (php, html, css, mySQL)</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau</h4>
                    <ul>
                        <li>Administration réseau (gestion des accès – supervision…)</li>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),

                (<p><h2>PARCOURS BTS SIO (EN OPTION AVEC SOUSCRIPTION DU PACK)</h2>
                    <ul>
                        <li>Algorithmique et mathématiques</li>
                        <li>Analyse économique, managériale et juridique</li>
                        <li>Culture et expression de la langue française</li>
                        <li>Projets personnalisés encadrés (PPE)</li>
                    </ul>
                    <h4>STAGE EN ENTREPRISE</h4>
                    <ul>Stage de 4 à 5 semaines</ul>
                    <h4>COMPÉTENCES À DÉVELOPPER</h4>
                    <ul>
                        <li>Gérer des incidents et problèmes (assistance utilisateur)</li>
                        <li>Développer une solution d’infrastructure</li>
                        <li>Développer et tester une application Objet</li>
                        <li>Développer une application web ou nomade</li>
                        <li>Administrer et superviser une solution d’infrastructure</li>
                        <li>Produire une documentation technique</li>
                    </ul>
                    <h2>Validation de la deuxième année + en option validation du BTS SIO Services Informatiques aux
                        organisations</h2>
                    <ul>
                        <li>option SLAM (Solutions logicielles et Applications métiers)</li>
                        <li>option SISR (Solutions d’infrastructure, systèmes et réseaux)</li>
                    </ul>
                </p>)
            ],
            proMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Langages de développement Objet et Tests unitaires (C++, C#)</li>
                        <li>Conception d’IHM web et nomade (PHP symfony, javascript, Jquery, UX Design…)</li>
                        <li>Langages de développement web (php, html, css, mySQL)</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau</h4>
                    <ul>
                        <li>Administration réseau (gestion des accès – supervision…)</li>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Environnement économique et juridique</li>
                        <li>Administration systèmes (shell Linux – Active directory…)</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),

                (<p><h2>PARCOURS BTS SIO (EN OPTION AVEC SOUSCRIPTION DU PACK)</h2>
                    <ul>
                        <li>Algorithmique et mathématiques</li>
                        <li>Analyse économique, managériale et juridique</li>
                        <li>Culture et expression de la langue française</li>
                        <li>Projets personnalisés encadrés (PPE)</li>
                    </ul>
                    <h4>STAGE EN ENTREPRISE</h4>
                    <ul>Stage de 4 à 5 semaines</ul>
                    <h4>COMPÉTENCES À DÉVELOPPER</h4>
                    <ul>
                        <li>Gérer des incidents et problèmes (assistance utilisateur)</li>
                        <li>Développer une solution d’infrastructure</li>
                        <li>Développer et tester une application Objet</li>
                        <li>Développer une application web ou nomade</li>
                        <li>Administrer et superviser une solution d’infrastructure</li>
                        <li>Produire une documentation technique</li>
                    </ul>
                    <h2>Validation de la deuxième année + en option validation du BTS SIO Services Informatiques aux
                        organisations</h2>
                    <ul>
                        <li>option SLAM (Solutions logicielles et Applications métiers)</li>
                        <li>option SISR (Solutions d’infrastructure, systèmes et réseaux)</li>
                    </ul>
                </p>)
            ]
        },
        btn3: {
            style: {
                top: "40%",
                left: "30%"
            },

            buttonText: 'BACHELOR 3',
            studentMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Le développement environnement Objet : Java et JEE</li>
                        <li>Intégration continue Java et Jenkins</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau</h4>
                    <ul>
                        <li>IP, technologie et service réseau sans fil</li>
                        <li>Administration sous Windows</li>
                        <li>Sécurité système et réseaux</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Conception et exploitation d’une base de données: SGBD Oracle</li>
                        <li>Administration d’une base de données</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Projet S.I. et recherche opérationnelle (UML, gestion de projet via ITIL et RO)</li>
                        <li>Environnement juridique et financier</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Sécurité informatique</li>
                        <li>Virtualisation</li>
                        <li>Données et objets connectés</li>
                    </ul>
                    <h2>CHOIX DU PARCOURS METIERS</h2>
                    <ul>
                        <li>Conception de solutions applicatives</li>
                        <li>Conception de solutions d’infrastructure</li>
                    </ul>
                    <h2>STAGE (3 À 6 MOIS) OU ALTERNANCE EN ENTREPRISE</h2>
                    <h2>Validation du BAC +3</h2>
                    <ul>
                        <li>Titre certifié niveau II « Administrateur Système Réseaux et Bases de Données » délivré par
                            l’ADIP-IPI, publié au Journal Officiel 07/06/2016
                        </li>
                        <li>Titre certifié niveau II « Concepteur développeur informatique » publié au Journal Officiel
                            19/02/2013
                        </li>
                    </ul>
                </p>)
            ],
            proMessage: [
                (<p><h2>PARCOURS SOCLE DEVOPS</h2>
                    <h4>Bloc de Compétences n° 1 – Développement d’applications informatiques</h4>
                    <ul>
                        <li>Le développement environnement Objet : Java et JEE</li>
                        <li>Intégration continue Java et Jenkins</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Administration Infrastructure système & réseau</h4>
                    <ul>
                        <li>IP, technologie et service réseau sans fil</li>
                        <li>Administration sous Windows</li>
                        <li>Sécurité système et réseaux</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion des données</h4>
                    <ul>
                        <li>Conception et exploitation d’une base de données: SGBD Oracle</li>
                        <li>Administration d’une base de données</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Méthodes et projets</h4>
                    <ul>
                        <li>Projet S.I. et recherche opérationnelle (UML, gestion de projet via ITIL et RO)</li>
                        <li>Environnement juridique et financier</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Communication</h4>
                    <ul>
                        <li>Anglais</li>
                        <li>Techniques d’Expression et Communication</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Sécurité informatique</li>
                        <li>Virtualisation</li>
                        <li>Données et objets connectés</li>
                    </ul>
                    <h2>CHOIX DU PARCOURS METIERS</h2>
                    <ul>
                        <li>Conception de solutions applicatives</li>
                        <li>Conception de solutions d’infrastructure</li>
                    </ul>
                    <h2>STAGE (3 À 6 MOIS) OU ALTERNANCE EN ENTREPRISE</h2>
                    <h2>Validation du BAC +3</h2>
                    <ul>
                        <li>Titre certifié niveau II « Administrateur Système Réseaux et Bases de Données » délivré par
                            l’ADIP-IPI, publié au Journal Officiel 07/06/2016
                        </li>
                        <li>Titre certifié niveau II « Concepteur développeur informatique » publié au Journal Officiel
                            19/02/2013
                        </li>
                    </ul>
                </p>)
            ]
        },
        btn4: {
            style: {
                top: "55%",
                left: "30%"
            },

            buttonText: 'INGENIERIE 4',
            studentMessage: [
                (<p><h2>PARCOURS SOCLE EXPERTISE INFORMATIQUE ET SYSTÈME D’INFORMATION</h2>
                    <h4>Bloc de Compétences n°1 – Fonction d’Encadrement</h4>
                    <ul>
                        <li>Stratégie financière</li>
                        <li>Anglais</li>
                        <li>Techniques d’expression et Communication</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Méthodes & Projet </h4>
                    <ul>
                        <li>Méthodes Agiles</li>
                        <li>Mathématiques et recherche opérationnelle</li>
                        <li>Planification et suivi de projets</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion de Données & Business Intelligence</h4>
                    <ul>
                        <li>Modélisation décisionnelle</li>
                        <li>NoSQL</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Etudes & Développement</h4>
                    <ul>
                        <li>Mobilité et systèmes embarqués</li>
                        <li>Intelligence artificielle</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Infrastructure Systèmes & Réseaux</h4>
                    <ul>
                        <li>Virtualisation et cloud computing</li>
                        <li>Sécurité infrastructure</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Réseaux et systèmes</li>
                        <li>Sécurité informatique et Système d’information</li>
                        <li>Business Intelligence et Big Data</li>
                    </ul>
                    <h2>STAGE (3 MOIS) OU ALTERNANCE EN ENTREPRISE </h2>
                    <h2>Validation du BAC +4</h2></p>)
            ],
            proMessage: [
                (<p><h2>PARCOURS SOCLE EXPERTISE INFORMATIQUE ET SYSTÈME D’INFORMATION</h2>
                    <h4>Bloc de Compétences n°1 – Fonction d’Encadrement</h4>
                    <ul>
                        <li>Stratégie financière</li>
                        <li>Anglais</li>
                        <li>Techniques d’expression et Communication</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Méthodes & Projet </h4>
                    <ul>
                        <li>Méthodes Agiles</li>
                        <li>Mathématiques et recherche opérationnelle</li>
                        <li>Planification et suivi de projets</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion de Données & Business Intelligence</h4>
                    <ul>
                        <li>Modélisation décisionnelle</li>
                        <li>NoSQL</li>
                    </ul>
                    <h4>Bloc de Compétences n°4 – Etudes & Développement</h4>
                    <ul>
                        <li>Mobilité et systèmes embarqués</li>
                        <li>Intelligence artificielle</li>
                    </ul>
                    <h4>Bloc de Compétences n°5 – Infrastructure Systèmes & Réseaux</h4>
                    <ul>
                        <li>Virtualisation et cloud computing</li>
                        <li>Sécurité infrastructure</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Réseaux et systèmes</li>
                        <li>Sécurité informatique et Système d’information</li>
                        <li>Business Intelligence et Big Data</li>
                    </ul>
                    <h2>STAGE (3 MOIS) OU ALTERNANCE EN ENTREPRISE </h2>
                    <h2>Validation du BAC +4</h2></p>)
            ]
        },
        btn5: {
            style: {
                top: "70%",
                left: "30%"
            },

            buttonText: 'INGENIERIE 5',
            studentMessage: [
                (<p><h2>PARCOURS SOCLE EXPERTISE INFORMATIQUE ET SYSTÈME D’INFORMATION</h2>
                    <h4>Bloc de Compétences n°1 – Fonction d’Encadrement</h4>
                    <ul>
                        <li>Management des systèmes d’information</li>
                        <li>Management des ressources</li>
                        <li>Anglais</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Méthodes & Projet </h4>
                    <ul>
                        <li>Management de projets</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion de Données & Business Intelligence</h4>
                    <ul>
                        <li>Qualité de données</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Management du cloud computing</li>
                        <li>Big data & l’internet des objets</li>
                        <li>Management des environnements nomades</li>
                        <li>Management de la sécurité informatique</li>
                    </ul>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Bloc de Compétences n° 4 – Études & Développement</li>
                        <li>Bloc de Compétences n° 5 – Infrastructure Systèmes & Réseaux</li>
                        <li>Bloc de Compétences n° 6 – Management et Conseil ERP</li>
                    </ul>
                    <h2>STAGE (6MOIS) OU ALTERNANCE EN ENTREPRISE + MÉMOIRE PROFESSIONNEL </h2>
                    <ul>TITRE RNCP – NIVEAU I – validation du BAC+5 Titre RNCP Niveau I « Expert en Informatique et
                        Système d’information » publié au Journal Officiel 07/06/2016.
                    </ul>
                    <ul>CERTIFICAT DE COMPÉTENCES PAR PARCOURS METIERS</ul>
                    <ul>
                        <li>Etudes & développement</li>
                        <li>Infrastructure systèmes & réseaux</li>
                        <li>Management & conseil ERP</li>
                    </ul>
                </p>)
            ],
            proMessage: [
                (<p><h2>PARCOURS SOCLE EXPERTISE INFORMATIQUE ET SYSTÈME D’INFORMATION</h2>
                    <h4>Bloc de Compétences n°1 – Fonction d’Encadrement</h4>
                    <ul>
                        <li>Management des systèmes d’information</li>
                        <li>Management des ressources</li>
                        <li>Anglais</li>
                    </ul>
                    <h4>Bloc de Compétences n°2 – Méthodes & Projet </h4>
                    <ul>
                        <li>Management de projets</li>
                    </ul>
                    <h4>Bloc de Compétences n°3 – Gestion de Données & Business Intelligence</h4>
                    <ul>
                        <li>Qualité de données</li>
                    </ul>
                </p>),
                (<p>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Management du cloud computing</li>
                        <li>Big data & l’internet des objets</li>
                        <li>Management des environnements nomades</li>
                        <li>Management de la sécurité informatique</li>
                    </ul>
                    <h2>CHOIX DU PARCOURS COMPLÉMENTAIRE</h2>
                    <ul>
                        <li>Bloc de Compétences n° 4 – Études & Développement</li>
                        <li>Bloc de Compétences n° 5 – Infrastructure Systèmes & Réseaux</li>
                        <li>Bloc de Compétences n° 6 – Management et Conseil ERP</li>
                    </ul>
                    <h2>STAGE (6MOIS) OU ALTERNANCE EN ENTREPRISE + MÉMOIRE PROFESSIONNEL </h2>
                    <ul>TITRE RNCP – NIVEAU I – validation du BAC+5 Titre RNCP Niveau I « Expert en Informatique et
                        Système d’information » publié au Journal Officiel 07/06/2016.
                    </ul>
                    <ul>CERTIFICAT DE COMPÉTENCES PAR PARCOURS METIERS</ul>
                    <ul>
                        <li>Etudes & développement</li>
                        <li>Infrastructure systèmes & réseaux</li>
                        <li>Management & conseil ERP</li>
                    </ul>
                </p>)
            ]
        },
        format: {
            btn: [
                "btn1", "btn2", "btn3", "btn4", "btn5"
            ]
        }
    },

    mydil: {
        image: require('../images/MyDIL.jpg'),
        btn1: {
            style: {
                top: "20%",
                left: "40%",
                width: "208px",
            },
            buttonText: 'Présentation',
            studentMessage: [(<p>
                <h2>Un lieu où le savoir à la demande est à la portée de tous nos élèves et où le matériel modulable et
                    high-tech est mis au service de l’expérimentation pédagogique.</h2>
                Échanger, développer, accélérer, co-construire, favoriser l’émergence de projets innovants par un lieu
                de collaboration et de confrontation unique entre étudiants :

                Espace, laboratoire dédié au codage, à l’innovation technologique.
            </p>)],
        },

        btn2: {
            style: {
                top: "40%",
                left: "50%",
                width: "100px",
            },
            buttonText: 'Bac à sable',
            studentMessage: [(<p>
                <h2>Le bac à sable est animé par un coach</h2>
                C'est un lieu de rendez vous pour les geeks, de formation et d’initiation aux outils informatiques et
                numériques, et de réflexion et d’expérimentation des nouveaux usages
            </p>)],
        },

        btn3: {
            style: {
                top: "40%",
                left: "37%",
                width: "100px",
            },
            buttonText: 'accès illimité aux dernières technologies ',
            studentMessage: [(<p>
                <h2>Dans le, MyDil vous avez un accès illimité aux dernières technologies </h2>
                <ul>
                    <li>NAO, le robot humanoïde sur lequel vous inventez le compagnon de demain,</li>
                    <li>L’oculus rift et sa nouvelle forme d’immersion en 3D,</li>
                    <li>Les nouvelles technologies de réalité augmentée (hololens…),</li>
                    <li>L’impression 3D pour le prototypage,</li>
                    <li>Les objets connectés d’aujourd’hui, et surtout ceux de demain, bracelets, montres, balance
                        tissu…
                    </li>
                    <li>Les nouvelles interfaces homme/machine, comme le leap motion,</li>
                    <li>Les différentes formes de tablettes et Smartphones nécessaires au développement des applications
                        du futur,
                    </li>
                    <li>Les technologies de système embarqué (raspberry py, arduino…),</li>
                    <li>Les drônes et leurs nouvelles applications,</li>
                    <li>Des outils collaboratifs innovants comme la surface hub,</li>
                    <li>Et bien d'autres !</li>
                </ul>
            </p>)],
        },
        format: {
            btn: [
                "btn1", "btn2", "btn3"
            ]
        }
    },

    admission: {
        image: require('../images/Bureau Administratif.jpg'),
        btn1: {
            buttonText: "Intégrer l'EPSI",
            style: {
                top: "40%",
                left: "60%",
                width: "100px"
            },
            studentMessage: [
                (<p>
                    <h2>L’acquisition de compétences : le coeur de notre système d’admission </h2>
                    Le programme modulaire, basé sur l’acquisition des compétences du parcours Ingénierie Informatique,
                    permet
                    toutes les admissions post-Bac, Bac+2, Bac+3 et sous conditions, les admissions parallèles.
                    Notre outil d’évaluation des compétences vous permet d’intégrer l’EPSI tout au long de l’année.
                    Salariés, demandeurs d’emplois, vous pouvez également intégrer l’EPSI et bénéficier de la VAE
                    (Validation
                    des Acquis et de l’Expérience).
                    Les formations de l’EPSI sont éligibles au CPF (Compte Personnel de Formation).
                </p>),
                (<p>
                    <h2>Tarifs :</h2>
                    <ul>Bachelor 1
                        <li>Concours 90€</li>
                        <li>Scolarité 6690€</li>
                    </ul>
                    <ul>Bachelor 2
                        <li>Concours 90€</li>
                        <li>Scolarité 6690€</li>
                    </ul>
                    <ul>Bachelor 3
                        <li>Concours 90€</li>
                        <li>Scolarité 8290€</li>
                    </ul>
                    <ul>Ingénierie 4
                        <li>Concours 100€</li>
                        <li>Scolarité 8690€</li>
                    </ul>
                    <ul>Ingénierie 5
                        <li>Concours 125€</li>
                        <li>Scolarité 9000€</li>
                    </ul>
                </p>)
            ]
        },
        btn2: {
            buttonText: "Financer ses études",
            style: {
                top: "40%",
                left: "40%",
                width: "100px"
            },
            studentMessage: [
                (<p>
                    <h2>My Budget, la bonne solution pour vous accompagner dans le finacnement de vos études</h2>
                    Candidat(e), admis(e) ou inscrit(e) dans nos écoles membres de HEP EDUCATION, My Budget vous propose
                    un ensemble de solutions pour vous accompagner dans le financement de vos études.
                    Le dispositif My Budget vous propose une offre évolutive comprenant des accords bancaires nationaux
                    avec offres privilèges, une aide au financement et au cautionnement de votre logement, des bourses
                    internes ou encore de régions et enfin des modalités de frais de scolarité adaptées à votre
                    situation
                    et besoins.
                    Pour plus d’informations, rendez-vous sur notre<a
                    href="http://www.hep-education.com/the-good-place-to-study/"> site </a>ou demandez conseil à
                    l’interlocuteur dédié au sein de votre futur établissement.
                </p>)
            ]
        },
        btn3: {
            buttonText: "Les services",
            style: {
                top: "30%",
                left: "50%",
                width: "100px"
            },
            studentMessage: [
                (<p>
                    <h2>My Studapart</h2>
                    C'est une plateforme de logement – location, colocation, sous-location – exclusivement réservée aux
                    étudiants des écoles membres de HEP EDUCATION.
                    Que ce soit pour un logement en France ou à l’étranger (Europe dans un premier temps, puis
                    Etats-Unis
                    et Chine à terme), en proximité de l’un de nos campus ou d’un prochain stage ou séjour à
                    l’international, cette plateforme permet aux étudiants de trouver ou proposer :
                    <ul>
                        <li>une location</li>
                        <li>des colocataires pour former des colocations</li>
                        <li>une sous-location</li>
                        <li>une chambre temporaire</li>
                    </ul>
                    <br/>
                    2 façons de se connecter à l’aide des identifiants e-campus :
                    <br/><br/>
                    Directement sur https://epsi.my-studapart.com/fr/
                    <br/><br/>
                    Via chaque e-campus, rubrique « Mes Services » (en ressaisissant les identifiants)
                    A noter : My Studapart est un service offert par HEP EDUCATION aux étudiants de l’ensemble de ses
                    écoles membres. HEP EDUCATION ne tire aucun bénéfice financier du fonctionnement de cette
                    plateforme.
                </p>),
                (<p>
                    <h2>My Campus Store, la plateforme de « Ventes privées » spéciale Etudiants </h2>
                    My Campus Store est la première plateforme de « Ventes Privées » exclusivement réservée aux
                    étudiants des écoles membres de HEP EDUCATION.
                    <br/><br/>
                    Chaque étudiant peut se connecter à cette plateforme et profiter de nombreux bons plans à prix
                    réduits – presse, high-tech, gastronomie, voyages, sport… Les offres peuvent être nationales
                    (achat en ligne de produits) ou en proximité de nos campus notamment pour la restauration, les
                    services et les spectacles.
                    L’offre propose aussi d’une sélection de « couponing » utilisable dans les commerces de
                    proximité.
                    2 façons de se connecter à l’aide des identifiants e-campus :
                    <ul>
                        <li>Directement sur www.my-campus-store.com</li>
                        <li>Via chaque e-campus, rubrique « Mes Services »</li>
                    </ul>
                    <br/><br/>
                    A noter : My Campus Store est un service offert par HEP EDUCATION aux étudiants de l’ensemble de
                    ses écoles membres. HEP EDUCATION ne tire aucun bénéfice financier du fonctionnement de cette
                    plateforme.
                </p>),
                (<p>
                    <h2>My Studapart, la plateforme logement spéciale Etudiants</h2>
                    My Studapart est une plateforme de logement – location, colocation, sous-location –
                    exclusivement
                    réservée aux étudiants des écoles membres de HEP EDUCATION.
                    <br/><br/>
                    Que ce soit pour un logement en France ou à l’étranger (Europe dans un premier temps, puis
                    Etats-Unis et Chine à terme), en proximité de l’un de nos campus ou d’un prochain stage ou
                    séjour
                    à l’international, cette plateforme permet aux étudiants de trouver ou proposer :
                    <br/><br/>
                    <ul>
                        <li>une location</li>
                        <li>des colocataires pour former des colocations</li>
                        <li>une sous-location</li>
                        <li>une chambre temporaire</li>
                    </ul>
                    A noter : My Studapart est un service offert par HEP EDUCATION aux étudiants de l’ensemble de
                    ses écoles membres. HEP EDUCATION ne tire aucun bénéfice financier du fonctionnement de cette
                    plateforme.
                </p>)
            ]
        },
        format: {
            btn: [
                "btn1", "btn2", "btn3"
            ]
        }
    },

    partnership: {
        image: require('../images/Partenaires 2.jpg'),
        btn1: {

            buttonText: "Formation en alternance",
            style: {
                top: "68%",
                left: "62%",
                width: "100px",
            },
            studentMessage: [(<p>
                <h2>Votre formation informatique en Alternance avec l’EPSI</h2>
                A l’EPSI, il n’y a pas de frontière entre la formation et le monde professionnel.

                Notre volonté est de former des Experts en Informatique directement opérationnels.

                <h2>L’alternance à l’EPSI, c’est possible !</h2>
                Pour répondre à la demande croissante des entreprises et des apprenants, l’EPSI propose un cursus en
                alternance.

                A partir de la 3ème année, les apprenants ont la possibilité d’alterner cours à l’EPSI et missions en
                entreprise pendant une année de formation.

                Véritable passeport de compétences, l’alternance est une formule qui permet d’allier la théorie à la
                pratique, de faciliter l’insertion professionnelle et de financer ses études.


            </p>),
                (<p><h2>L’alternance à l’EPSI c’est :</h2>
                    <ul>
                        <li>10 000 entreprises partenaires : résolument tournée vers le monde de l’entreprise, l’EPSI
                            entretient des liens forts avec de nombreuses entreprises régionales et nationales.
                        </li>
                        <li>Un suivi personnalisé pour chaque apprenant: une aide à la recherche d’entreprise, à la
                            rédaction du CV, lettre de motivation, entretien.
                        </li>
                        <li>Développement d’un projet professionnel : l’EPSI favorise la réussite de chacun en trouvant
                            des missions en entreprise qui vous correspondent et vous font progresser.
                        </li>
                        <li>Une aide au montage du contrat : l’école aide les entreprises au recrutement et participe au
                            montage du contrat.
                        </li>
                    </ul>
                </p>),
                (<p><h2>Différents types de contrats en alternance sont possibles pour les apprenants: </h2>
                    <ul>
                        <li>Le contrat de professionnalisation</li>
                        <li>Le contrat de partenariat école / entreprise.</li>
                        <li>Le contrat apprentissage (uniquement à EPSI Campus de Paris)</li>
                    </ul>
                </p>),
            ],

            proMessage: [(<p>
                <h2>RECRUTER UN ALTERNANT :</h2>
                VOUS AVEZ DES BESOINS EN RECRUTEMENT ? NOUS AVONS DES PROFILS COMPÉTENTS !
                <br/><br/><h3>Stages :</h3>
                <ul>
                    <li>De bac à bac +5</li>
                    <li>Périodes comprises entre 2 et 6 mois</li>
                    <li>Cadre législatif : convention de stage</li>
                </ul>

                <h3>Alternance :</h3>
                <ul>
                    <li>De bac + 3 à bac + 5</li>
                    <li>Cadre législatif : contrat de professionnalisation, contrat de partenariat école entreprise
                        (CPEE), contrat d’apprentissage
                    </li>
                </ul>

                <h3>Jobs étudiants :</h3>
                <ul>
                    <li>Nos jeunes peuvent être en recherche de jobs étudiants.</li>
                    <li>N’hésitez pas à nous faire parvenir vos offres qui leurs seront transmises.</li>
                </ul>

            </p>)]
        },
        format: {
            btn: [
                "btn1"
            ]
        }
    },
};